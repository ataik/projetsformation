def retcode = 'unknown'

pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                dir('gs-serving-web-content') {
                    ansiColor('xterm') {
                        sh "mvn clean"
                        sh "mvn package"
                    }
                }
                dir('skills-manager') {
                    ansiColor('xterm') {
                        sh "mvn clean"
                        sh "mvn package"
                    }
                }
            }
        }
        stage('Test') {
            steps {
                dir('gs-serving-web-content') {
                    ansiColor('xterm') {
                        sh "mvn test"
                    }
                }
                dir('skills-manager') {
                    ansiColor('xterm') {
                        sh "mvn test"
                    }
                }
            }
        }
        stage('Merge with Develop') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'bitbucket-creds', passwordVariable: 'BITBUCKET_PWD', usernameVariable: 'BITBUCKET_USER')]) {
                    sh '''
                        git clone https://$BITBUCKET_USER:$BITBUCKET_PWD@bitbucket.org/phas006/projetsformation.git
                        cd projetsformation
                        git checkout develop
                        git fetch origin $BRANCH_NAME
                        git merge remotes/origin/$BRANCH_NAME -m "Jenkins: Automatic merge after test success (Build no $BUILD_NUMBER)"
                        git push
                    '''
                }
             }
        }
        stage('Post Pull Request') {
            agent {
                dockerfile {
                    filename 'Dockerfile.python'
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'bitbucket-creds', passwordVariable: 'BITBUCKET_PWD', usernameVariable: 'BITBUCKET_USER')]) {
                    script {
                        dir ('jenkins') {
                            retcode = sh(script: "python pullrequest_check.py $BITBUCKET_USER $BITBUCKET_PWD develop master projetsformation", returnStatus: true)
                            if (retcode == 0) {
                                sh "echo '/// Pull Request Found!! '"
                            } else if (retcode == 1) {
                                sh '''
                                    echo "/// Pull Request Not Found!! Clear to trigger it"
                                    python post.py $BITBUCKET_USER $BITBUCKET_PWD "Automated Pull Request triggered by Build no $BUILD_NUMBER" develop master projetsformation
                                '''
                            } else if (retcode == 2) {
                                sh "pwd"
                                sh "ls -l"
                                sh "echo '/// Http 404, Something wrong with config'"
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}