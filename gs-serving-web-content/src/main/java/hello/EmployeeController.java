package hello;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import model.Employee;

@Controller
public class EmployeeController {

    @GetMapping("/employees")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		List<Employee> employeeList = getEmployeeList();
		model.addAttribute("employeeList", employeeList);
		
		return "employeeList";
    }
    
    /**
     * Get employee list with his skills by calling the REST web service
     * @return
     */
    private List<Employee> getEmployeeList() {
    	RestTemplate restTemplate = new RestTemplate();
    	
    	ResponseEntity<List<Employee>> response = restTemplate.exchange(
    			  								"http://backend:9090/employees/",
    			  								HttpMethod.GET, null,
    			  								new ParameterizedTypeReference<List<Employee>>(){});
    	
    	List<Employee> employeeList = response.getBody();
    	
    	return employeeList;
    }
}
